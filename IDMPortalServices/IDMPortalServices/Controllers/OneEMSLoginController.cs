﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Philips.HealthCare.IDM.Portal.DataObjects;
using Philips.HealthCare.IDM.Portal.Logger;

namespace Philips.HealthCare.IDM.Portal.Services.Controllers
{
    public class OneEMSLoginController : ApiController
    {
        ResponseObject responseObject;
        public HttpResponseMessage Post([FromBody] string postData)
        {
            responseObject = new ResponseObject();
            try
            {
                LogManager.LogWriter.WriteLog(new LogMessage() { Description = "Login to sales force" });

                string oneEMSUser = ConfigurationManager.AppSettings["OneEMSEIIUser"];
                string oneEMSPass = ConfigurationManager.AppSettings["OneEMSEIIPass"];
                string sessionID = string.Empty;
                
                if (string.Compare(ConfigurationManager.AppSettings["EIIOneEMSWebReference"].ToString()
                                  , "PRODUCTION"
                                  , true) == 0)
                {
                    OneEMS_ServiceInterface.OneEMSLogin oneEMSLogin = new OneEMS_ServiceInterface.OneEMSLogin();
                    oneEMSLogin.login(oneEMSUser, oneEMSPass);
                    sessionID = oneEMSLogin.getSessionID;
                }
                else
                {
                    OneEMS_ServiceInterfaceTest.OneEMSLogin oneEMSLogin = new OneEMS_ServiceInterfaceTest.OneEMSLogin();
                    oneEMSLogin.login(oneEMSUser, oneEMSPass);
                    sessionID = oneEMSLogin.getSessionID;
                }

                responseObject.status = 1;
                responseObject.message = "success";
                responseObject.payload = sessionID;
            }
            catch (Exception ex)
            {
                responseObject.status = 0;
                responseObject.message = ex.Message;
                responseObject.payload = string.Empty;
            }

            return new HttpResponseMessage()
            {
                Content = new JsonContent(JObject.Parse(JsonConvert.SerializeObject(responseObject)))
            };
        }
    }
}