﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Philips.HealthCare.IDM.Portal.DataAccess;
using Philips.HealthCare.IDM.Portal.Logger;
using Newtonsoft.Json.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;
using MongoDB.Bson;
using System.Net.Http.Formatting;
using Philips.HealthCare.IDM.Portal.DataObjects;
using System.Data;

namespace Philips.HealthCare.IDM.Portal.Services.Controllers
{
    public class ExceptionController : ApiController
    {
        /// <summary>
        /// Site Match
        /// </summary>
        /// <param name="siteid"></param>
        /// <param name="matchtype"></param>
        /// <returns></returns>
        [AcceptVerbs("GET")]
        [ActionName("SiteMatch")]
        public HttpResponseMessage SiteMatch(string siteid, string matchtype)
        {
            List<SiteInfo> sites = new List<SiteInfo>();
            ResponseObject responseObject = new ResponseObject();
            try
            {
                LogManager.LogWriter.WriteLog(new LogMessage() { Description = "get method with parameter of SWD Controller" });
                DataSet ds = HBDBHelper.GetPossibleSiteMatch(siteid, matchtype);
                
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    SiteInfo site = new SiteInfo
                    {
                        siteid = item["SIT_KEY"].ToString(),
                        sitename = item["SIT_NAME"].ToString(),
                        siteversion = item["SIT_VERSION"].ToString(),
                        country = new Country {
                            country = item["RegionName"].ToString()
                        },
                        sitestatus = item["SiteStatus"].ToString()
                    };
                    sites.Add(site);
                }
                responseObject.status = 1;
                responseObject.message = "SiteException";
                responseObject.payload = JsonConvert.SerializeObject(sites);
            }
            catch (Exception ex)
            {
                responseObject.status = 0;
                responseObject.message = ex.Message;
                responseObject.payload = string.Empty;
            }

            return new HttpResponseMessage()
            {
                Content = new JsonContent(JObject.Parse(JsonConvert.SerializeObject(responseObject)))
            };
        }

        /// <summary>
        /// Gets all the Site Exceptions from the IDM Ecosystem
        /// </summary>
        /// <param name="exptype"></param>
        /// <returns>HttpResponseMessage</returns>
        [AcceptVerbs("GET")]
        [ActionName("Exceptions")]
        public HttpResponseMessage Exceptions(string exptype)
        {
            List<SiteInfo> sites = new List<SiteInfo>();
            ResponseObject responseObject = new ResponseObject();
            try
            {
                responseObject.status = 1;
                responseObject.message = "Exceptions";
                responseObject.payload = "{\"siteexception\" : [\"PIT00\", \"BHEA1\", \"UCHHS\", \"UPMC0\"]}".ToString();
            }
            catch (Exception ex)
            {
                responseObject.status = 0;
                responseObject.message = ex.Message;
                responseObject.payload = string.Empty;
            }

            return new HttpResponseMessage()
            {
                Content = new JsonContent(JObject.Parse(JsonConvert.SerializeObject(responseObject)))
            };
        }

        /// <summary>
        /// Creates a new Site Exception message
        /// </summary>
        /// <param name="jsonArray">JsonStringArray</param>
        /// <returns>HttpResponseMessage</returns>
        [AcceptVerbs("POST")]
        [ActionName("SiteExpRequest")]      
        public HttpResponseMessage SiteExpRequest([FromBody] string value)
        {
            HttpResponseMessage response;
            ResponseObject responseObject; ;
            responseObject = new ResponseObject();
            //JsonStringArray jsonArray = "{}";
            try
            {
                SiteExceptionReqMsg siteExpRequest = JsonConvert.DeserializeObject<SiteExceptionReqMsg>(value);
                if (siteExpRequest == null)
                {
                    throw new Exception("Invalid Site Exception Request.");
                }
                response = EcosystemAPIHelper.PostSiteExceptionRequest(value);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    // helper.SaveExpSiteRequest(siteExpRequest);
                    responseObject.status = 1;
                    responseObject.message = "Site Exception Request submitted successfully.";
                    responseObject.payload = JsonConvert.SerializeObject(siteExpRequest);
                }
                else
                {
                    responseObject.status = 0;
                    responseObject.message = "Error in submitting Site Exception Request to the IDM Ecosystem.";
                    responseObject.payload = "Success";
                }
            }
            catch (Exception ex)
            {
                responseObject.status = 0;
                responseObject.message = ex.Message;
                responseObject.payload = string.Empty;
            }

            return new HttpResponseMessage()
            {
                Content = new JsonContent(JObject.Parse(JsonConvert.SerializeObject(responseObject)))
            };
        }
    }
}