﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Philips.HealthCare.IDM.Portal.DataAccess;
using Philips.HealthCare.IDM.Portal.Logger;
using Newtonsoft.Json.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;
using MongoDB.Bson;
using System.Net.Http.Formatting;
using Philips.HealthCare.IDM.Portal.DataObjects;

namespace Philips.HealthCare.IDM.Portal.Services.Controllers
{
    public class SitesController : ApiController
    {

        MongoDbHelper helper;

        /// <summary>
        /// Initializes a new instance of the <see cref="SitesController"/> class.
        /// </summary>
        public SitesController()
        {
            helper = new MongoDbHelper();
        }

        // GET api/<controller>
        /// <summary>
        /// Gets this instance.
        /// </summary>
        /// <returns></returns>
        public HttpResponseMessage Get()
        {
            LogManager.LogWriter.WriteLog(new LogMessage() { Description = "get method of Sites Controller" });
            var json = JArray.Parse(helper.GetAllSites().ToJson());
            return new HttpResponseMessage()
            {
                Content = new JsonContent(json)
            };
        }

        [AcceptVerbs("GET")]
        public HttpResponseMessage Nodes(string Id)
        {
            LogManager.LogWriter.WriteLog(new LogMessage() { Description = "get nodes with id of Sites Controller" });
            var json = JArray.Parse(helper.GetAllNodesForSite(Id));
            return new HttpResponseMessage()
            {
                Content = new JsonContent(json)
            };
        }


        [AcceptVerbs("GET")]
        public HttpResponseMessage Messages(string Id)
        {
            LogManager.LogWriter.WriteLog(new LogMessage() { Description = "get messages with id of Sites Controller" });
            var json = JArray.Parse(helper.GetAllMessagesForSite(Id));
            return new HttpResponseMessage()
            {
                Content = new JsonContent(json)
            };
        }

        [AcceptVerbs("POST")]
        public HttpResponseMessage SitesForPackage([FromBody] string packagename)
        {
            LogManager.LogWriter.WriteLog(new LogMessage() { Description = "Data Retreived for Service Events" });
            JsonMediaTypeFormatter objFormatter = new JsonMediaTypeFormatter();

            PackageRuleHelper pkgHelper = new PackageRuleHelper();
            PackageRule pkgRule = pkgHelper.GetPackageRulesByName(packagename);

            //var res = helper.GetSitesByCoutry(pkgRule.countries);
            //var httpResponseMessage = new HttpResponseMessage()
            //{
            //    Content = new ObjectContent(res.GetType(), res, objFormatter)
            //};
            //LogManager.LogWriter.WriteLog(new LogMessage() { Description = "Data returned for Service Events" });
            //return httpResponseMessage;
            
            JArray json;

            if (pkgRule != null && pkgRule.countries != null && pkgRule.countries.Count > 0)
            {

                json = JArray.Parse(helper.GetSitesByCoutry(pkgRule.countries).ToJson());
            }
            else
            {
                json = JArray.Parse("[]");
            }
            return new HttpResponseMessage()
            {
                Content = new JsonContent(json)
            };
        }

        //// POST api/<controller>
        //public void Post([FromBody]string value) {
        //}

        //// PUT api/<controller>/5
        //public void Put(int id, [FromBody]string value) {
        //}

        //// DELETE api/<controller>/5
        //public void Delete(int id) {
        //}
    }

    public class JsonContent : HttpContent
    {
        private readonly JToken _value;

        public JsonContent(JToken value)
        {
            _value = value;
            Headers.ContentType = new MediaTypeHeaderValue("application/json");
        }

        protected override Task SerializeToStreamAsync(Stream stream,
            TransportContext context)
        {
            var jw = new JsonTextWriter(new StreamWriter(stream))
            {
                Formatting = Formatting.Indented
            };
            _value.WriteTo(jw);
            jw.Flush();
            return Task.FromResult<object>(null);
        }

        protected override bool TryComputeLength(out long length)
        {
            length = -1;
            return false;
        }
    }
}