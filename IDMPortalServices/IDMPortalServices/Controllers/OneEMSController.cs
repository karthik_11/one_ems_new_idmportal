﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Philips.HealthCare.IDM.Portal.DataAccess;
using Philips.HealthCare.IDM.Portal.DataObjects;
using Philips.HealthCare.IDM.Portal.Logger;
using Philips.HealthCare.IDM.Portal.OneEMS_ServiceInterface;
using Philips.HealthCare.IDM.Portal.OneEMS_ServiceInterfaceTest;
using System.Net;
using System.Net.Http.Formatting;

namespace Philips.HealthCare.IDM.Portal.Services.Controllers
{
    public class OneEMSController : ApiController
    {
        OneEMSDBHelper helper;
        ResponseObject responseObject;

        /// <summary>
        /// Initializes a new instance of the <see cref="SystemFiltersController"/> class.
        /// </summary>
        public OneEMSController()
        {
            helper = new OneEMSDBHelper();
        }

        public HttpResponseMessage Post([FromBody] OneEMSCase objCase)
        {
            string output = string.Empty;
            responseObject = new ResponseObject();
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            //System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 ;
            try
            {
                if (objCase == null
                    || string.IsNullOrEmpty(objCase.siteid)
                    || string.IsNullOrEmpty(objCase.hostname)
                    || string.IsNullOrEmpty(objCase.service)
                    || string.IsNullOrEmpty(objCase.status)
                    || string.IsNullOrEmpty(objCase.technical_id))
                {
                    throw new Exception("Invalid case metadata passed");
                }

                objCase.priority = "4-Intermittent Problem";

                if (CreateOneEMSCase(objCase, out output))
                {
                    double casenum = 0;
                    if (double.TryParse(output, out casenum))
                    {
                        objCase.casenum = output;
                        objCase.casedate = DateTime.UtcNow;
                        objCase.oneemsstatus = "Open";
                    }
                    else
                    {
                        throw new Exception("Invalid case number returned");
                    }
                    helper.InsertOneEMSCase(objCase);
                    responseObject.status = 1;
                    responseObject.message = "success";
                    responseObject.payload = Convert.ToString(output);
                }
                else
                {
                    throw new Exception(output);
                }
            }
            catch (Exception ex)
            {
                responseObject.status = 0;
                responseObject.message = ex.Message;
                responseObject.payload = string.Empty;
            }

            return new HttpResponseMessage()
            {
                Content = new JsonContent(JObject.Parse(JsonConvert.SerializeObject(responseObject)))
            };
        }

        protected bool CreateOneEMSCase(OneEMSCase objCase, out string output)
        {
            string oneEMSUser = ConfigurationManager.AppSettings["OneEMSEIIUser"];
            string oneEMSPass = ConfigurationManager.AppSettings["OneEMSEIIPass"];
            string returnVal = string.Empty;
            output = string.Empty;
            try
            {
                if (string.Compare(ConfigurationManager.AppSettings["EIIOneEMSWebReference"].ToString()
                                  , "PRODUCTION"
                                  , true) == 0)
                {
                    OneEMS_ServiceInterface.OneEMSLogin oneEMSLogin = 
                        new OneEMS_ServiceInterface.OneEMSLogin();
                    OneEMS_ServiceInterface.OneEMSCreateCase oneEMSCreateCase = 
                        new OneEMS_ServiceInterface.OneEMSCreateCase();

                    oneEMSLogin.login(oneEMSUser, oneEMSPass);

                    //string techID = objCase.siteid;
                    //string techID = ConfigurationManager.AppSettings["EIIOneEMSDevTechID"];

                    returnVal = oneEMSCreateCase.createCase(objCase.technical_id,
                        objCase.service,
                        objCase.output,
                        objCase.priority,
                        oneEMSLogin.getSessionID,
                        oneEMSLogin.getSessionURL);
                }
                else
                {
                    OneEMS_ServiceInterfaceTest.OneEMSLogin oneEMSLogin = 
                        new OneEMS_ServiceInterfaceTest.OneEMSLogin();
                    OneEMS_ServiceInterfaceTest.OneEMSCreateCase oneEMSCreateCase = 
                        new OneEMS_ServiceInterfaceTest.OneEMSCreateCase();

                    oneEMSLogin.login(oneEMSUser, oneEMSPass);

                    //string techID = ConfigurationManager.AppSettings["EIIOneEMSDevTechID"];

                    returnVal = oneEMSCreateCase.createCase(objCase.technical_id,
                        objCase.service,
                        objCase.output,
                        objCase.priority,
                        oneEMSLogin.getSessionID,
                        oneEMSLogin.getSessionURL);
                }

                if (returnVal != null)
                {
                    string caseVal = string.Empty;
                    string result = ErrorFromOneEms(returnVal);

                    if (returnVal == "0")
                    {
                        caseVal = "Zero value returned";
                    }
                    else if (result.Substring(0, 5).Equals("ERROR"))
                    {
                        if (result.Substring(5, 1).Equals("1"))
                            caseVal = "Missing Mandatory input CaseTitle.";
                        else if (result.Substring(5, 1).Equals("2"))
                            caseVal = "Mandatory input CaseNotes.";
                        else if (result.Substring(5, 1).Equals("3"))
                            caseVal = "Missing Mandatory input Priority.";
                        else if (result.Substring(5, 1).Equals("4"))
                            caseVal = "Unable to locate Equipment with Technical ID";
                        else if (result.Substring(5, 1).Equals("5"))
                            caseVal = "Unable to locate customer associated with Equipment Technical Id";
                        else if (result.Substring(5, 1).Equals("6"))
                            caseVal = "Could not find 'EI Proactive' User";
                        else if (result.Substring(5, 1).Equals("7"))
                            caseVal = "Invalid Request";

                        throw new Exception(caseVal);
                    }
                    else
                    {
                        output = returnVal;
                    }
                }
                else
                {
                    throw new Exception("An error occured while trying to create the case. " +
                        "Please try to create a case again. If the issue persists, " +
                        "please create the case manually and use the link button to link to that " +
                        "OneEMS case. Also, forward this error to " + System.Configuration.ConfigurationManager.AppSettings["Support_Email_ID"]);
                }

                return true;
            }
            catch (Exception ex)
            {
                output = ex.Message;
            }
            return false;
        }

        /// <summary>
        /// Decode error generated while creating OneEMS case
        /// </summary>
        /// <param name="emsReturnVal"></param>
        /// <returns></returns>
        protected static string ErrorFromOneEms(string emsReturnVal)
        {
            string emsVal = emsReturnVal;

            if (emsReturnVal.Equals("Missing Mandatory input CaseTitle."))
                return "ERROR:1";
            else if (emsReturnVal.Equals("Mandatory input CaseNotes."))
                return "ERROR:2";
            else if (emsReturnVal.Equals("Missing Mandatory input Priority."))
                return "ERROR:3";
            else if (emsReturnVal.Equals("Unable to locate Equipment with Technical ID"))
                return "ERROR:4";
            else if (emsReturnVal.Equals("Unable to locate customer associated with Equipment Technical Id"))
                return "ERROR:5";
            else if (emsReturnVal.Equals("Could not find 'EI Proactive' User"))
                return "ERROR:6";
            else if (emsReturnVal.Equals("Invalid Request"))
                return "ERROR:7";
            else
                return emsVal;

        }
    }
}