﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using Philips.HealthCare.IDM.Portal.DataAccess;
using Philips.HealthCare.IDM.Portal.DataObjects;
using Philips.HealthCare.IDM.Portal.Logger;
using System.Net;


namespace Philips.HealthCare.IDM.Portal.Services.Controllers {
    public class ServiceEventsController : ApiController {
        MongoDbHelper helper;        

        /// <summary>
        /// Initializes a new instance of the <see cref="MessagesController"/> class.
        /// </summary>
         public ServiceEventsController() {
            
            helper = new MongoDbHelper();
        }

        /// <summary>
        /// Get
        /// </summary>
        /// <returns>all the CRITICAL/WARNING/UNKNOWN messages</returns>
         public HttpResponseMessage Get() {
             JsonMediaTypeFormatter objFormatter = new JsonMediaTypeFormatter();
             List<Message> result = helper.GetDefaultServiceEvents();

             var res = from w1 in result
                       join w2 in StaticCache.GetSites() on w1.sitekey equals w2.siteid
                       join w3 in StaticCache.GetAllNamespaces() on w1.servicenamespace equals w3.servicenamespace
                       select new Message
                       {
                           sitekey = w1.sitekey,
                           sitename = w2.sitename,
                           version = w2.siteversion,
                           caseid = w1.caseid,
                           casetype = w1.casetype,
                           description = w1.description,
                           duration = w1.duration,
                           durationdt = w1.durationdt,
                           elementid = w1.elementid,
                           eventfilter = w3.eventfilter,
                           hostipaddress = w1.hostipaddress,
                           hostname = w1.hostname,
                           lastreceived = w1.lastreceived,
                           namespaceabbreviation = w3.displayname,
                           servicenamespace = w3.servicenamespace,
                           status = w1.status,
                           type = w1.type
                       };

             var httpResponseMessage = new HttpResponseMessage()
             {
                 Content = new ObjectContent(res.GetType(), res, objFormatter)
             };

             return httpResponseMessage;
         }

        /// <summary>
         /// FilteredMessage
        /// </summary>
         /// <param name="criteria"></param>
         /// <returns>all the messages based upon the criteria</returns>
         public HttpResponseMessage Post([FromBody] SearchCriteria criteria) {
             LogManager.LogWriter.WriteLog(new LogMessage() { Description = "Post method of Service EvetController" });
             JsonMediaTypeFormatter objFormatter = new JsonMediaTypeFormatter();
             List<Message> result = helper.FilteredServiceEvents(criteria);

             var res = from w1 in result
                       join w2 in StaticCache.GetSites() on w1.sitekey equals w2.siteid
                       join w3 in StaticCache.GetAllNamespaces() on w1.servicenamespace equals w3.servicenamespace
                       select new Message
                       {
                           sitekey = w1.sitekey,
                           sitename = w2.sitename,
                           version = w2.siteversion,
                           caseid = w1.caseid,
                           casetype = w1.casetype,
                           description = w1.description,
                           duration = w1.duration,
                           durationdt = w1.durationdt,
                           elementid = w1.elementid,
                           eventfilter = w3.eventfilter,
                           hostipaddress = w1.hostipaddress,
                           hostname = w1.hostname,
                           lastreceived = w1.lastreceived,
                           namespaceabbreviation = w3.displayname,
                           servicenamespace = w3.servicenamespace,
                           status = w1.status,
                           type = w1.type
                       };

             LogManager.LogWriter.WriteLog(new LogMessage() { Description = "Data Retreived for Service Events" });

             var httpResponseMessage = new HttpResponseMessage()
             {
                 Content = new ObjectContent(res.GetType(), res, objFormatter)
             };
             LogManager.LogWriter.WriteLog(new LogMessage() { Description = "Data returned for Service Events" });
             return httpResponseMessage;
         }
    }
}