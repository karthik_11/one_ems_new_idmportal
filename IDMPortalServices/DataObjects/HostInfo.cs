﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Philips.HealthCare.IDM.Portal.DataObjects {
    /// <summary>
    /// HostInfo
    /// </summary>
    public class HostInfo {
        /// <summary>
        /// hostname
        /// </summary>
        public string hostname { get; set; }
    }
}
