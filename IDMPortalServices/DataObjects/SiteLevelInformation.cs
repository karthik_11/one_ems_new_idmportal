﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Philips.HealthCare.IDM.Portal.DataObjects.Model {
    /// <summary>
    /// SiteLevelInformation
    /// </summary>
    public class SiteLevelInformation {
        /// <summary>
        /// Siteid
        /// </summary>
        public string SiteID { get; set; }
        /// <summary>
        /// Version
        /// </summary>
        public string Version { get; set; }
        /// <summary>
        /// Hostname
        /// </summary>
        public string HostName { get; set; }
        /// <summary>
        /// HostIP
        /// </summary>
        public string HostIP { get; set; }
        /// <summary>
        /// TotalCount
        /// </summary>
        public int TotalCount { get; set; }
        /// <summary>
        /// SuccessCount
        /// </summary>
        public int SuccessCount { get; set; }
        /// <summary>
        /// FailureCount
        /// </summary>
        public int FailureCount { get; set; }
    }
}
